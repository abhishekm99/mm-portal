module.exports = (req,res,next) => {
    if(req.user.utype === 'member') {
        return res.redirect('/admin/dashboard');
    }
    else
    {
        next();
    }
}