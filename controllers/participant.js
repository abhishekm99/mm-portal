const Fivek = require('../models/registeredmem');
const Paytm = require('../models/paytm');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const fs = require('fs');
const qr = require('qrcode');
const uuidv4 = require('uuid/v4');



exports.approveParticipant = (req,res,next) => {
    reg_id = req.body.reg_id;
    console.log(reg_id)
    Fivek.findOne({where: {Registration_Id: reg_id}}).then(result => {
        result.update({approved: 1});
        req.flash('success', 'User has been approved');
        return res.redirect(`/admin/approved`);
    })
    .catch(err => {
        throw err
    })
}

exports.disapproveParticipant = (req,res,next) => {
    reg_id = req.body.reg_id;
    console.log(reg_id)
    Fivek.findOne({where: {Registration_Id: reg_id}}).then(result => {
        result.update({approved: 0});
        req.flash('error', 'User disapproved');
        return res.redirect(`/admin/dashboard`);
    })
    .catch(err => {
        throw err
    })
}

exports.disqualifyParticipant = (req,res,next) => {
    reg_id = req.body.reg_id;
    req_reas = req.body.reas_disqual;
    console.log(reg_id)
    Fivek.findOne({where: {Registration_Id: reg_id}}).then(result => {
        result.update({approved: 2, reas: req_reas});
        req.flash('error', 'User disqualified');
        return res.redirect(`/admin/dashboard`);
    })
    .catch(err => {
        throw err
    })
}

exports.flagParticipant = (req,res,next) => {
    reg_id = req.body.reg_id;
    req_reas = req.body.reas_disqual;
    console.log(reg_id);
    Fivek.findOne({where: {Registration_Id: reg_id}}).then(result => {
        result.update({approved: 3, reas: req_reas});
        req.flash('error', 'User Flagged');
        return res.redirect(`/admin/dashboard`);
    })
    .catch(err => {
        throw err
    })
}

exports.getSearchParticipant = (req,res,next) => {
    let { searchq } = req.query;
    console.log(searchq)
    if(!searchq || searchq===''){
        page = req.query.p;
        limit = 50;
        if(!page){
            page=1;
        }
        offset = (page*limit)-limit;
        if(req.user.utype === 'super'){
            Fivek.findAndCountAll({limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                pages = Math.ceil(products.count/limit);
                return res.render('admin/searchdash', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true});
            })
        }
        Fivek.findAndCountAll({where: {Ticket_Name: req.user.rtype},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
            pages = Math.ceil(products.count/limit);
            return res.render('admin/searchdash', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true});
        })
    }
    else
    {
        let query;
        if(req.user.utype === 'super'){
            query = `select * from fivektimeds WHERE (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
        else{
            query = `select * from fivektimeds WHERE (Ticket_Name = '${req.user.rtype}') and (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
        
            db.query(query, '%' + searchq + '%', (err, result)=> {
                if(err) throw err;
                else{
                    products=result;
                    res.render('admin/searchdash', {
                        items:products,
                        hasitems: products.length>0,
                        title: 'Clubs Search',
                        notMain: true,
                    })
                }
           })
        // console.log('here at the wall');
        // Fivek.findAll({
        //     where: Sequelize.where(
        //       Sequelize.fn("CONCAT",
        //         Sequelize.col("Attendee_Name"),
        //         " ",
        //         Sequelize.col("Registration_Id"),
        //         " ",
        //         Sequelize.col("Employee_Number")
        //       ),
        //       {
        //         eq: searchq
        //       }
        //     )
        // }).then(results => {
        //     console.log(results);
        //     pages = Math.ceil(results.count/limit);
        //     res.render('admin/searchdash', {items: results.rows, title:'Search',pages:pages,hasitems: results.rows.length>0, currentPage: page});
        // })
    }
}

exports.getApprovedParticipant = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
        page = req.query.p;
        limit = 50;
        if(!page){
            page=1;
        }
        offset = (page*limit)-limit;
        //Need to fix this part
        if(req.user.utype === 'super'){
            Fivek.findAndCountAll({where: {approved : 1},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                pages = Math.ceil(products.count/limit);
                return res.render('admin/approved', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true, errors: req.flash('error'), success: req.flash('success')});
            })
        }
        else{
            Fivek.findAndCountAll({where: {approved : 1, Ticket_Name: req.user.rtype},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                pages = Math.ceil(products.count/limit);
                return res.render('admin/approved', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true,errors: req.flash('error'),
                success: req.flash('success')});
            })
        }   
    }
    else
    {
        let query;
        if(req.user.utype='super'){
            query = `select * from fivektimeds WHERE (approved = true) and (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
        else{
            query = `select * from fivektimeds WHERE (approved = true) and (Ticket_Name = '${req.user.rtype}') and (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
            db.query(query, '%' + searchapprovedq + '%', (err, result)=> {
                if(err) throw err;
                else{
                    products=result;
                    res.render('admin/approved', {
                        items:products,
                        hasitems: products.length>0,
                        title: 'Clubs Search',
                        notMain: true,
                        errors: req.flash('error'),
                        success: req.flash('success'),
                    })
                }
           })
        // console.log('here at the wall');
        // Fivek.findAll({
        //     where: Sequelize.where(
        //       Sequelize.fn("CONCAT",
        //         Sequelize.col("Attendee_Name"),
        //         " ",
        //         Sequelize.col("Registration_Id"),
        //         " ",
        //         Sequelize.col("Employee_Number")
        //       ),
        //       {
        //         eq: searchq
        //       }
        //     )
        // }).then(results => {
        //     console.log(results);
        //     pages = Math.ceil(results.count/limit);
        //     res.render('admin/searchdash', {items: results.rows, title:'Search',pages:pages,hasitems: results.rows.length>0, currentPage: page});
        // })
    }
}

exports.getFlaggedParticipant = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
        page = req.query.p;
        limit = 50;
        if(!page){
            page=1;
        }
        offset = (page*limit)-limit;
        //Need to fix this part
        if(req.user.utype === 'super'){
            Fivek.findAndCountAll({where: {approved : 3},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                pages = Math.ceil(products.count/limit);
                return res.render('admin/flagged', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true, errors: req.flash('error'), success: req.flash('success')});
            })
        }
        else{
            Fivek.findAndCountAll({where: {approved : 3, Ticket_Name: req.user.rtype},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                pages = Math.ceil(products.count/limit);
                return res.render('admin/flagged', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true,errors: req.flash('error'),
                success: req.flash('success')});
            })
        }
    }
    else
    {
        let query;
        if(req.user.utype='super'){
            query = `select * from fivektimeds WHERE (approved = 3) and (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
        else{
            query = `select * from fivektimeds WHERE (approved = 3) and (Ticket_Name = '${req.user.rtype}') and (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
            db.query(query, '%' + searchapprovedq + '%', (err, result)=> {
                if(err) throw err;
                else{
                    products=result;
                    res.render('admin/flagged', {
                        items:products,
                        hasitems: products.length>0,
                        title: 'Clubs Search',
                        notMain: true,
                        errors: req.flash('error'),
                        success: req.flash('success'),
                    })
                }
           })
        // console.log('here at the wall');
        // Fivek.findAll({
        //     where: Sequelize.where(
        //       Sequelize.fn("CONCAT",
        //         Sequelize.col("Attendee_Name"),
        //         " ",
        //         Sequelize.col("Registration_Id"),
        //         " ",
        //         Sequelize.col("Employee_Number")
        //       ),
        //       {
        //         eq: searchq
        //       }
        //     )
        // }).then(results => {
        //     console.log(results);
        //     pages = Math.ceil(results.count/limit);
        //     res.render('admin/searchdash', {items: results.rows, title:'Search',pages:pages,hasitems: results.rows.length>0, currentPage: page});
        // })
    }
}

exports.getDisqualifiedParticipant = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
        page = req.query.p;
        limit = 50;
        if(!page){
            page=1;
        }
        offset = (page*limit)-limit;
        //Need to fix this part
        if(req.user.utype === 'super'){
            Fivek.findAndCountAll({where: {approved : 2},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                pages = Math.ceil(products.count/limit);
                return res.render('admin/disqualified', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true, errors: req.flash('error'), success: req.flash('success')});
            })
        }else{
            Fivek.findAndCountAll({where: {approved : 2, Ticket_Name: req.user.rtype},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                pages = Math.ceil(products.count/limit);
                return res.render('admin/disqualified', {items: products.rows, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page, notMain: true,errors: req.flash('error'),
                success: req.flash('success')});
            })
        }
    }
    else
    {
        let query;
        if(req.user.utype='super'){
            query = `select * from fivektimeds WHERE (approved = 2) and (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
        else{
            query = `select * from fivektimeds WHERE (approved = 2) and (Ticket_Name = '${req.user.rtype}') and (concat(Attendee_Name,Registration_Id,Employee_Number) like ?)`;
        }
            db.query(query, '%' + searchapprovedq + '%', (err, result)=> {
                if(err) throw err;
                else{
                    products=result;
                    res.render('admin/disqualified', {
                        items:products,
                        hasitems: products.length>0,
                        title: 'Clubs Search',
                        notMain: true,
                        errors: req.flash('error'),
                        success: req.flash('success'),
                    })
                }
           })
        // console.log('here at the wall');
        // Fivek.findAll({
        //     where: Sequelize.where(
        //       Sequelize.fn("CONCAT",
        //         Sequelize.col("Attendee_Name"),
        //         " ",
        //         Sequelize.col("Registration_Id"),
        //         " ",
        //         Sequelize.col("Employee_Number")
        //       ),
        //       {
        //         eq: searchq
        //       }
        //     )
        // }).then(results => {
        //     console.log(results);
        //     pages = Math.ceil(results.count/limit);
        //     res.render('admin/searchdash', {items: results.rows, title:'Search',pages:pages,hasitems: results.rows.length>0, currentPage: page});
        // })
    }
}

exports.getParticipantDetails = (req,res,next) => {
    const regno = req.params.bib;
    Fivek.findByPk(regno).then(result => {
        res.render('admin/profile', {
            title:'Profile card',
            profile : result,
            notMain: true,
        })
        .catch(err => console.log(err));
        
    })
}

exports.getMAbove56 = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Age: {
                    [Op.gte]: 56
                },Gender:'Male'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getFAbove56 = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Age: {
                    [Op.gte]: 56
                },Gender:'Female'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getM35To55 = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Age: {
                    [Op.between]: [35, 55]
                },Gender:'Male'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getF35To55 = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Age: {
                    [Op.between]: [35, 55]
                },Gender:'Female'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getOMale = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Gender:'Male'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getOFemale = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Gender:'Female'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getO5k = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Ticket_Name: '5km'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getO10k = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Ticket_Name: '10km'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getO21k = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Ticket_Name: 'HALF MARATHON (21.095km)'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.getO42k = (req,res,next) => {
    let { searchapprovedq } = req.query;
    if(!searchapprovedq || searchapprovedq===''){
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            //Need to fix this part
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where: {Ticket_Name: 'FULL MARATHON (42.195km)'},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.json({products: products, pages: pages});
            })
        }
    }
}

exports.retryPayment = (req,res,next) => {
    const oid = req.query.oid;
    Fivek.findOne({where:{Order_Id:oid}}).then(result => {
        if(result.dataValues.paymentStatus != 0){
            req.flash('errors','Payment already confirmed please contact administrator');
            return res.redirect('/admin/dashboard');
        }
        else{
            const newoid = uuidv4();
            result.Order_Id = newoid;
            return result.save();
        }
    }).then(result => {
            console.log(result);
            var amount;
            if(result.dataValues.maheval == 1){
                amount = 10;
            }
            else{
                amount = 20;
            }
            Paytm.create({
            Order_Id: result.dataValues.Order_Id,
            Cust_Id: result.dataValues.Registration_Id,
            Amount: amount,
            Contact: result.dataValues.Contact_Number,
            Email: result.dataValues.Attendee_Email,
        }).then(result => {
            res.redirect(`http://localhost:3000/payment?orderid=${result.dataValues.Order_Id}&callback=http://localhost:3000/paytmCallback`)
        })
    }).catch(err => console.log(err));
}