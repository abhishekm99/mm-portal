const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const nodemailer = require('nodemailer')
const mailGun = require('nodemailer-mailgun-transport');
const User = require('../../models/users');
const Fivek = require('../../models/registeredmem');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const fs = require('fs');
const qr = require('qrcode');
const { validationResult } = require('express-validator/check')

const auth = {
    auth: {
        api_key: process.env.API_KEY, // TODO: 
        domain: process.env.DOMAIN // TODO:
    }
};
let transporter = nodemailer.createTransport( mailGun(auth) );

exports.getLogin = (req,res,next) => {
        res.render('admin/login', {
            pageTitle: 'Login',
            isAuthenticated: req.session.isLoggedIn,
            errors: req.flash('error'),
            title:'Login',
            notMain: true,
        });
}

exports.postLogin = (req,res,next) => {
    const username = req.body.username;
    const password = req.body.pass;
    User.findOne({where: {email: username}})
        .then(user => {
            if(!user){
                req.flash('error', 'Invalid email or password');
                return res.redirect(`/admin/login`);
            }
            bcrypt.compare(password, user.password).then(result => {
                if(result){
                    req.session.isLoggedIn = true;
                    req.session.user = user;                        
                    return req.session.save(err => {
                        console.log(err);
                    res.redirect('/admin/dashboard');
            })
                }
                else{ 
                    req.flash('error', 'Invalid email or password');
                    return res.redirect(`/admin/login`);
                }
            }).catch(err => 
            {
                console.log(err);
                return res.redirect(`/admin/login`);
            })
        })
    .catch(err => console.log(err));
}

exports.getRegister = (req,res,next) => {
            res.render('admin/register', {
                pageTitle: "Sign Up",
                isAuthenticated: req.session.isLoggedIn,
                errors: req.flash('error'),
                title: 'Register',
                notMain: true,
            })
}

exports.postRegister = (req,res,next) => {
    auth1 = req.body.auth1;
    auth2 = req.body.auth2;
    var condition; 
    if(req.body.rtype == 'super'){
        condition = auth1===`${process.env.superauth1}` && auth2===`${process.env.superauth2}`
    }
    else{
        condition = auth1===`${process.env.auth1}` && auth2===`${process.env.auth2}`
    }
    if(condition){
        console.log('entered',`${process.env.auth1}`,`${process.env.auth2}`,auth1,auth2);
            const username = req.body.username;
            const email = req.body.email;
            const password = req.body.pass;
            const rtype = req.body.rtype;
            const cnfpass = req.body.cnfpass;
            let message = req.flash('error');
            const errors = validationResult(req);
            var utype;
            if(rtype == 'super'){
                utype = 'super'
            }
            else{
                utype = 'organizer'
            }
            if(!errors.isEmpty()){
                return res.status(422).render('admin/register', {
                    pageTitle: "Sign Up",
                    isAuthenticated: req.session.isLoggedIn,
                    errors : errors.array()[0].msg,
                    oldInput: {email:email},
                    nothome: true,
                })
            }
            if(message.length>0){
                message = message[0];
            }
            else{
                message = null;
            }
            User.findOne({where:{username:username}}).then(userDoc => {
                if(userDoc){
                    req.flash('error', 'User name already exists, please use another one');
                    return res.redirect(`/admin/register`);
                }
                return bcrypt.hash(password, 12)
                    .then(hashedpassword => {
                        const user = User.create({
                            username: username,
                            email: email,
                            password:hashedpassword,
                            rtype: rtype,
                            isLoggedin: 0,
                            utype:utype,
                            createdAt: new Date(),
                })
                return user;
                })
                .then(res.redirect(`/admin/dashboard`))
            })
            .catch(err => console.log(err));
    }
    else{
        req.flash('error', 'Unauthorised Regsitration');
        return res.redirect(`/admin/register`);
    }
}

exports.postLogout = (req,res,next) => {
    req.session.destroy(err=> {
        console.log(err);
        res.redirect('/admin/login')
    });
}


exports.getDashboard = (req,res,next) => {
        var url;
                if(process.env.ENVIRONMENT === 'PRODUCTION'){
                    url = 'https://manipalmarathon.in';
                }
                else{
                    url = 'http://localhost:4001';
                }
        if(req.user.utype === 'member'){
                Fivek.findByPk(req.user.regid).then(result => {
                    /* var qrdata = {
                    name: result.Attendee_Name,
                    age: result.Age,
                    regid: result.Registration_Id,
                    email: result.Attendee_Email,
                    rtype: result.Ticket_Name,
                    contact: result.Contact_Number,
                    institute: result.Institution_Name,
                    reg: result.Employee_Number,
                } */
                qr.toDataURL(`${url}/admin/dashboard/${result.Registration_Id}`).then(url => {
                    return res.render('admin/profile', {
                        title:'Profile card',
                        profile : result,
                        notMain: true,
                        isMember: true,
                        url: url,
                    })
                }).catch(err => console.log(err));
            }).catch(err => console.log(err));
        }
        else{
            page = req.query.p;
            limit = 50;
            if(!page){
                page=1;
            }
            offset = (page*limit)-limit;
            if(req.user.utype === 'super'){
                Fivek.findAndCountAll({where:{approved:0},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.render('admin/dashboard', {items: products.rows,pages:pages,hasitems: products.rows.length>0, currentPage: page, isAuthenticated: req.session.isLoggedIn,
                        title:'Dashboard',
                        notMain: true,
                        errors: req.flash('error'),
                        success: req.flash('success'),
                        user: req.user
                    })
                })
            }
            else{
                Fivek.findAndCountAll({where: {approved: 0, Ticket_Name: req.user.rtype},limit:limit,offset:offset,order:[['Registration_Id','ASC']]}).then(products => {
                    pages = Math.ceil(products.count/limit);
                    return res.render('admin/dashboard', {items: products.rows,pages:pages,hasitems: products.rows.length>0, currentPage: page, isAuthenticated: req.session.isLoggedIn,
                        title:'Dashboard',
                        notMain: true,
                        errors: req.flash('error'),
                        success: req.flash('success'),
                        user: req.user
                    })
                })
            }
        }
}

exports.getFPwd = (req,res,next) => {
    res.render('admin/fpwd', {
        pageTitle: 'Reset Password',
        isAuthenticated: req.session.isLoggedIn,
        errors: req.flash('error'),
        title:'Reset Password',
        notMain: true,
    });
}

exports.postReset = (req,res,next) => {
    crypto.randomBytes(32, (err, buffer) => {
        if(err){
            console.log(err);
            return res.redirect('/admin/fpwd');
        }
        const token = buffer.toString('hex');
        console.log(req.body.username);
        User.findOne({where:{email:req.body.username}})
        .then(user => {
            if(!user){
                req.flash('error', 'No accounts found with the email');
                return res.redirect('/resetp');
            }
            console.log(user);
            user.resetToken = token;
            user.resetTokenExpiration = Date.now() + 3600000;
            return user.save();
        })
        .then(result => {
            var url;
            if(process.env.ENVIRONEMT === 'PRODUCTION'){
                url = 'https://manipalmarathon.in';
            }
            else{
                url = 'http://localhost:4001';
            }
            res.redirect('/admin/login');
            transporter.sendMail({
                to:req.body.username,
                from:'noreply@manipalmararthon.in',
                subject: 'Password Reset',
                html:`
                <div class="container">
    <div class="top" style="height:100px; background-color: #f2f2f2;">
        <center>
            <h2>Manipal Marathon</h2>
        </center>
    </div>
    <div class="midd" style="height:300px">
        <center>
            <h3 style="font-style: italic;color: #919191;">Hello from Manipal Marathon!</h3>
            <h1 style="font-weight: bold;">Password Reset Instructions</h1>
            <p>Someone requested that the password be reset for your account</p>
            <p>If this was a mistake, you are requested to bring it to the notice of the organising committee</p>
            <p>To reset your password, visit the following address:</p>
            <a href="${url}/admin/resetp/${token}">Click here to reset your password</a>
        </center>
    </div>
</div>
                `,
            });
        })
        .catch(err => console.log(err))
    })
}

exports.getNewPassword = (req,res,next) => {
    const token = req.params.token;
    User.findOne({where:{resetToken: token, resetTokenExpiration:{[Op.gt]: Date.now()}}})
    .then(user => {
        let message = req.flash('error');
        if(message.length>0){
            message = message[0];
        }
        else{
            message = null;
        }
        res.render('admin/rpwd', {
            pageTitle: 'Reset Password',
            errors: message,
            userId: user.id,
            passwordToken: token,
        })
    })
    .catch(err => console.log(err));
}

exports.postNewPassword = (req,res,next) => {
    const newPassword = req.body.pass;
    const userId = req.body.userId;
    const passwordToken = req.body.token;

    let resetUser;

    User.findOne({where:{resetToken: passwordToken, resetTokenExpiration:{[Op.gt]:Date.now()}, id:userId}})
    .then(user => {
        resetUser = user;
        return bcrypt.hash(newPassword,12);
    })
    .then(hashedpassword => {
        resetUser.password = hashedpassword;
        resetUser.resetToken = undefined;
        resetUser.resetTokenExpiration = undefined;
        return resetUser.save();
    })
    .then(result => {
        res.redirect('/admin/login');
    })
    .catch(err => console.log(err));
}

exports.getScanner = (req,res,next) => {
    res.render('admin/scanner', {
        title: 'Scanner',
        notMain: true,
    })
}
