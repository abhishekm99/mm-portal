const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Fivek_timed = sequelize.define('fivektimed', {
    BiB_no: {
        type:Sequelize.STRING,
        autoIncrement: false,
        allowNull: true,
        primaryKey: false,
    },
    Registration_Id:{
        type:Sequelize.STRING,
        autoIncrement: false,
        allowNull: false,
        primaryKey: true,
    },
    Attendee_Name:{
        type:Sequelize.STRING,
        allowNull:true
    },
    Attendee_Email: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Ticket_Name: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Contact_Number: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Gender: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Age: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Institution_Name: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Employee_Number: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Emergency_Contact_Name: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Emergency_Contact_Number: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Order_Id: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    T_Shirt_Size: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    approved:{
        type:Sequelize.BOOLEAN,
        dafault: 0,
        allowNull:false,
    },
    reas: {
        type: Sequelize.TEXT,
        allowNull: true,
        default: 'none'
    }
},
{
    timestamps: false,
})

module.exports = Fivek_timed;