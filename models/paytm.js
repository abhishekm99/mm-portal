const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Paytm = sequelize.define('paytm', {
    Order_Id: {
        type:Sequelize.STRING,
        allowNull: false,
        primaryKey: true
    },
    Cust_Id:{
        type:Sequelize.STRING,
        allowNull:false
    },
    Amount:{
        type:Sequelize.STRING,
        allowNull:false
    },
    Contact:{
        type:Sequelize.STRING,
        allowNull:false
    },
    Email: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    created_at: {
        type:Sequelize.DATE,
        allowNull:true,
    },
    updated_at: {
        type:Sequelize.DATE,
        allowNull:true,
    },
    paytmid: {
        type:Sequelize.STRING,
        allowNull:true,
    },
    banktxnid: {
        type:Sequelize.STRING,
        allowNull:true,
    },
    status: {
        type:Sequelize.STRING,
        allowNull:true,
    },
    amount_paid: {
        type:Sequelize.STRING,
        allowNull:true,
    },
    txndate: {
        type:Sequelize.DATE,
        allowNull:true,
    },
},
{
    timestamps: false,
})

module.exports = Paytm;