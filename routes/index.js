var express = require('express');
var router = express.Router();
const mysql = require('mysql');

const dotenv = require('dotenv');

dotenv.config();

function handleDisconnect() {

  const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: `${process.env.MYSQL_PASS}`,
    database: 'marathon',
  });
  try{
  db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
  });
  global.db = db;
  }
  catch(err) {
    console.log(err);
  }
  
  
  db.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
  
  }
  
  
  handleDisconnect();

/* GET home page. */
router.get('/', function(req, res) {
  var isMember;
  if(req.user){
    if(req.user.utype === 'member'){
      isMember = true
    }
  }
  else{
    isMember = false
  }
  res.render('landing', { title: 'Manipal Marathon', isMember: isMember });
});

module.exports = router;
