var express = require('express');
var router = express.Router();
const path = require('path');
const rootDir = require('../helpers/path');

var loginController = require('../controllers/admin/login');
const participantController = require('../controllers/participant');
const isAuth = require('../middlewares/is-auth');
const isMember = require('../middlewares/is-member');
const { check, body } = require('express-validator/check');


router.get('/login', loginController.getLogin);
router.post('/login', loginController.postLogin);
router.get('/dashboard/:bib', isAuth, isMember, participantController.getParticipantDetails);
router.get('/dashboard', isAuth, loginController.getDashboard);
router.get('/searchq', isAuth, isMember, participantController.getSearchParticipant);
router.get('/approved', isAuth, isMember, participantController.getApprovedParticipant);
router.get('/flagged', isAuth, isMember, participantController.getFlaggedParticipant);
router.get('/disqualified', isMember, isAuth, participantController.getDisqualifiedParticipant);
router.get('/register',isAuth, isMember, loginController.getRegister);
router.post('/register', isMember, loginController.postRegister);
router.post('/dashboard', isAuth, isMember, participantController.approveParticipant);
router.post('/dashboard/dis', isAuth, isMember,  participantController.disapproveParticipant);
router.post('/dashboard/disqualify', isAuth, isMember, participantController.disqualifyParticipant);
router.post('/dashboard/flag', isAuth, isMember, participantController.flagParticipant);
router.get('/api/getmabove56', isAuth, isMember, participantController.getMAbove56);
router.get('/api/getfabove56', isAuth, isMember, participantController.getFAbove56);
router.get('/api/getm35to55', isAuth, isMember, participantController.getM35To55);
router.get('/api/getf35to55', isAuth, isMember, participantController.getF35To55);
router.get('/api/getomale', isAuth, isMember, participantController.getOMale);
router.get('/api/getofemale', isAuth, isMember, participantController.getOFemale);
router.get('/api/geto5k', isAuth, isMember, participantController.getO5k);
router.get('/api/geto10k', isAuth, isMember, participantController.getO10k);
router.get('/api/geto21k', isAuth, isMember, participantController.getO21k);
router.get('/api/geto42k', isAuth, isMember, participantController.getO42k);
router.get('/fpwd', loginController.getFPwd);
router.post('/fpwd', loginController.postReset);
router.get('/resetp/:token', loginController.getNewPassword);
router.post('/rpwd', loginController.postNewPassword);
router.get('/scanner', isAuth, isMember, loginController.getScanner)
router.get('/retrypayment', isAuth, participantController.retryPayment);


router.post('/logout', isAuth, loginController.postLogout);
module.exports = router;