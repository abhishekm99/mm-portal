$(document).ready(function(){
    var url;
    $('#mabove56').click(function() {
        url = '/admin/api/getmabove56';
        displaydata(url);
    })
    $('#fabove56').click(function() {
        url = '/admin/api/getfabove56';
        displaydata(url);
    })
    $('#m35to55').click(function() {
        url = '/admin/api/getm35to55';
        displaydata(url);
    })
    $('#f35to55').click(function() {
        url = '/admin/api/getf35to55';
        displaydata(url);
    })
    $('#omale').click(function() {
        url = '/admin/api/getomale';
        displaydata(url);
    })
    $('#ofemale').click(function() {
        url = '/admin/api/getofemale';
        displaydata(url);
    })

    $('#o5k').click(function() {
        url = '/admin/api/geto5k';
        displaydata(url);
    })
    $('#o10k').click(function() {
        url = '/admin/api/geto10k';
        displaydata(url);
    })
    $('#o21k').click(function() {
        url = '/admin/api/geto21k';
        displaydata(url);
    })

    $('#o42k').click(function() {
        url = '/admin/api/geto42k';
        displaydata(url);
    })

    function displaydata(urldata){
        var products;
        var pages;
        $.getJSON(urldata, function(data) {
            products = data.products.rows;
            pages = data.pages;
            $("#alldata").css("display", "none");
            $("#under55data").css("display", "block");
            $("#under55tbody").children().remove();
            for(i=0; i<products.length; i++){
                if(products[i].approved == 1){
                    $("#under55tbody").append(`
                    <tr class="approved">
                        <td>${products[i].BiB_no}</td>
                        <td>${products[i].Registration_Id}</td>
                        <td>${products[i].Attendee_Name}</td>
                        <td>${products[i].Employee_Number}</td>
                        <td>${products[i].Ticket_Name}</td>
                        <td>
                            <a class="btn btn btn-indigo btn-sm m-0" href="/admin/dashboard/${products[i].Registration_Id}">View</a>
                        </td>
                    </tr>
                    `)
                }
                else if(products[i].approved == 2){
                    $("#under55tbody").append(`
                    <tr class="disqual">
                        <td>${products[i].BiB_no}</td>
                        <td>${products[i].Registration_Id}</td>
                        <td>${products[i].Attendee_Name}</td>
                        <td>${products[i].Employee_Number}</td>
                        <td>${products[i].Ticket_Name}</td>
                        <td>
                            <a class="btn btn btn-indigo btn-sm m-0" href="/admin/dashboard/${products[i].Registration_Id}">View</a>
                        </td>
                    </tr>
                    `)
                }
                else if(products[i].approved == 3){
                    $("#under55tbody").append(`
                    <tr class="flagged">
                        <td>${products[i].BiB_no}</td>
                        <td>${products[i].Registration_Id}</td>
                        <td>${products[i].Attendee_Name}</td>
                        <td>${products[i].Employee_Number}</td>
                        <td>${products[i].Ticket_Name}</td>
                        <td>
                            <a class="btn btn btn-indigo btn-sm m-0" href="/admin/dashboard/${products[i].Registration_Id}">View</a>
                        </td>
                    </tr>
                    `)
                }
                else{
                $("#under55tbody").append(`
                <tr>
                    <td>${products[i].BiB_no}</td>
                    <td>${products[i].Registration_Id}</td>
                    <td>${products[i].Attendee_Name}</td>
                    <td>${products[i].Employee_Number}</td>
                    <td>${products[i].Ticket_Name}</td>
                    <td>
                        <a class="btn btn btn-indigo btn-sm m-0" href="/admin/dashboard/${products[i].Registration_Id}">View</a>
                    </td>
                </tr>
                `)
                }
            }
            var active;
            if(urldata.split('=')[1]){
                active = urldata.split('=')[1];
            }
            else{
                active = 1;
            }
            $('#data-pagination').children().remove();
            for(i=0;i<pages;i++){
                if(active == (i+1)){
                    $('#data-pagination').append(`
                    <li class="page-item active"><a data-value="${i+1}" href="javascript:;">${i+1}</a></li>
                `)
                }
                else{
                    $('#data-pagination').append(`
                    <li class="page-item"><a data-value="${i+1}" href="javascript:;">${i+1}</a></li>
                `)
                }
                
            } 
            $('.page-item').find('a').click(function(){
                var p = $(this).attr('data-value');
                urlnew = url + `?p=${p}`
                displaydata(urlnew);
            })       
        });
        
    }
    
})

// {{#ifCond @root.currentPage this}}active{{/ifCond}}