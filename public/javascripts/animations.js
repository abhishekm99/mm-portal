$(document).ready(function(){
    var isInViewport = function (elem) {
        var distance = elem.getBoundingClientRect();
        return (
            distance.top >= -200 &&
            distance.left >= 0 &&
            distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            distance.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    };

    TweenMax.to('.alert-container',1.5,{opacity:1, y:0, display:'flex'})
    window.addEventListener('scroll', function (event) {
        // if (isInViewport(landing_section_3)) {
        //     //
        // }
    }, false);

})