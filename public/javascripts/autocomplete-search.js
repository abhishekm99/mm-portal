$(document).ready(function(){
    //Search autocomplete method
    const search = document.getElementById('search');
    const matchList = document.getElementById('matchList');
    let array_data = [];
    $.get('/clubs/getSearchIndex?token=gkXjW4Dnjt0m2Steio3DwFdRVlmn0JCfXIETR68PlqwDEjqCv1E8kpQSVYiKD1JC', (datas) => {
        for(i=0;i<datas.length;i++){
            array_data.push(datas[i]);
        }
    });
    const searchStates = searchText => {
        let matches = array_data.filter(data => {
            const regex = new RegExp(`${searchText}`, 'gi');
            return data.match(regex);
        });
        if(searchText.length == 0){
            matches = [];
            matchList.innerHTML = '';
        }
        outputHtml(matches);
    }

    const outputHtml = matches => {
        let html;
        if(matches.length>0){
            html = matches.map(match => `
                <li class="card-body mb-1 named" data-value="${match}">
                ${match}
                </li>
            `).join('');
            matchList.innerHTML = html;
            
        }
        else if(search.value.length===0){
            matchList.innerHTML = '';
        }
        else{
            html = `
            <span class="card-body mb-1">No results found</span>
        `; 
        matchList.innerHTML = html;
        }
        
        $('.named').click(function() {
            var d = $(this).data('value');      
            search.value = d;
            matchList.innerHTML = '';
        });
    }

    search.addEventListener('input', () => searchStates(search.value))
})